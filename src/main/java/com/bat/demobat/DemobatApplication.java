package com.bat.demobat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemobatApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemobatApplication.class, args);
	}

}
